import textwrap
import unittest

from kooki.processor.metadata import Metadata, data_merge, parse_metadata


class TestMetadata(unittest.TestCase):
    def test_update_metadata_empty(self):
        metadata = Metadata()
        metadata.update()
        self.assertEqual(metadata, {})

    def test_update_metadata_dict_as_args(self):
        dict_one = {"key": "value"}
        metadata = Metadata()
        metadata.update(dict_one)
        self.assertEqual(metadata, {"key": "value"})

    def test_update_metadata_dict_as_args_multiple(self):
        dict_one = {"key_1": "value_1"}
        dict_two = {"key_2": "value_2"}
        metadata = Metadata()
        metadata.update(dict_one, dict_two)
        self.assertEqual(metadata, {"key_1": "value_1", "key_2": "value_2"})

    def test_update_metadata_dict_as_args_multiple_same(self):
        dict_one = {"key_1": "value_1"}
        dict_two = {"key_1": "value_2"}
        metadata = Metadata()
        metadata.update(dict_one, dict_two)
        self.assertEqual(metadata, {"key_1": "value_2"})

    def test_update_metadata_dict_as_kwargs(self):
        dict_one = {"key": "value"}
        metadata = Metadata()
        metadata.update(data=dict_one)
        self.assertEqual(metadata, {"data": {"key": "value"}})

    def test_update_metadata_dict_as_kwargs_multiple(self):
        dict_one = {"key_1": "value_1"}
        dict_two = {"key_2": "value_2"}
        metadata = Metadata()
        metadata.update(data_1=dict_one, data_2=dict_two)
        self.assertEqual(metadata, {
            "data_1": {
                "key_1": "value_1"
            },
            "data_2": {
                "key_2": "value_2"
            }
        })

    def test_update_metadata_dict_bad_type(self):
        with self.assertRaises(Exception):
            metadata = Metadata()
            metadata.update("string")

    def test_parse_metadata_bad(self):
        content = textwrap.dedent("""\
            trucmuche := doekdo
            """)
        metadata = {"path.truc": content}

        with self.assertRaises(Exception):
            parse_metadata(metadata)

    def test_parse_metadata_yaml(self):
        content = textwrap.dedent("""\
            key: value
            """)
        metadata = {"path.yml": content}

        result = parse_metadata(metadata)
        self.assertEqual(result, {"key": "value"})

    def test_parse_metadata_toml(self):
        content = textwrap.dedent("""\
            key = "value"
            """)
        metadata = {"path.toml": content}

        result = parse_metadata(metadata)
        self.assertEqual(result, {"key": "value"})

    def test_parse_metadata_json(self):
        content = textwrap.dedent("""\
            {
                "key": "value"
            }
            """)
        metadata = {"path.json": content}

        result = parse_metadata(metadata)
        self.assertEqual(result, {"key": "value"})

    def test_data_merge_string_to_string(self):
        data_1 = "data_1"
        data_2 = "data_2"
        result = data_merge(data_1, data_2)
        self.assertEqual(result, "data_2")

    def test_data_merge_list_to_list(self):
        data_1 = ["data_1"]
        data_2 = ["data_2"]
        result = data_merge(data_1, data_2)
        self.assertEqual(result, ["data_1", "data_2"])

    def test_data_merge_string_to_list(self):
        data_1 = ["data_1"]
        data_2 = "data_2"
        result = data_merge(data_1, data_2)
        self.assertEqual(result, ["data_1", "data_2"])

    def test_data_merge_dict_to_dict(self):
        data_1 = {"key_1": "data_1"}
        data_2 = {"key_2": "data_2"}
        result = data_merge(data_1, data_2)
        self.assertEqual(result, {"key_1": "data_1", "key_2": "data_2"})

    def test_data_merge_dict_to_dict_key_already_present(self):
        data_1 = {"key_1": "data_1"}
        data_2 = {"key_1": "data_2"}
        result = data_merge(data_1, data_2)
        self.assertEqual(result, {"key_1": "data_2"})

    def test_data_merge_string_to_dict(self):
        data_1 = {"key_1": "data_1"}
        data_2 = "data_2"
        with self.assertRaises(Exception):
            data_merge(data_1, data_2)

    def test_data_merge_unknow_to_dict(self):
        class Empty:
            pass

        data_1 = Empty()
        data_2 = {"key_2": "data_2"}
        with self.assertRaises(Exception):
            data_merge(data_1, data_2)
