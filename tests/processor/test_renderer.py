import textwrap
import unittest

from kooki.processor.renderer import render
from munch import Munch


class TestRenderer(unittest.TestCase):
    def setUp(self):
        self.extensions = Munch(
            {
                "__block_quote__": lambda renderer, text: text,
                "__block_html__": lambda renderer, html: html,
                "__inline_html__": lambda renderer, html: html,
                "__header__": lambda renderer, text, level, raw, uid: text,
                "__hrule__": lambda renderer: "---",
                "__list__": lambda renderer, body, ordered: "{}\n{}".format(
                    ordered, body
                ),
                "__list_item__": lambda renderer, text: text,
                "__paragraph__": lambda renderer, text: text,
                "__table__": lambda renderer, header, body: "{}\n{}".format(
                    header, body
                ),
                "__table_row__": lambda renderer, content, placeholder: content,
                "__table_cell__": lambda renderer, content, placeholder, align, header: content,
                "__link__": lambda renderer, link, title, content: "{}\n{}\n{}".format(
                    link, title, content
                ),
                "__autolink__": lambda renderer, link, is_email: "{}\n{}".format(
                    link, is_email
                ),
                "__block_code__": lambda renderer, code, language: "{}\n{}".format(
                    code, language
                ),
                "__codespan__": lambda renderer, text: text,
                "__double_emphasis__": lambda renderer, text: text,
                "__emphasis__": lambda renderer, text: text,
                "__image__": lambda renderer, src, title, alt_text: "{}\n{}\n{}".format(
                    src, title, alt_text
                ),
                "__strikethrough__": lambda renderer, text: text,
                "__text__": lambda renderer, text: text,
                "__linebreak__": lambda renderer: "linebreak",
                "__newline__": lambda renderer: "newline",
            }
        )

    def tearDown(self):
        pass

    def test_block_quote(self):
        content = "> block"
        should_be = """block\n\n"""
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_block_html(self):
        content = "<div>block</div>"
        should_be = """<div>block</div>"""
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_inline_html(self):
        content = "<span>inline</span>"
        should_be = """<span>inline</span>\n\n"""
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_header(self):
        content = "# header"
        should_be = """header"""
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_hrule(self):
        content = "---"
        should_be = """---"""
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_list(self):
        content = textwrap.dedent(
            """
            - item
            - item
        """
        )
        should_be = "False\nitemitem"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_ordered(self):
        content = textwrap.dedent(
            """
            1. item
            2. item
        """
        )
        should_be = "True\nitemitem"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_paragraph(self):
        content = "text"
        should_be = "text\n\n"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_table(self):
        content = textwrap.dedent(
            """
        | head | head |
        | ---- | ---- |
        | line | line |
        """
        )
        should_be = "headhead\nlineline"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_link(self):
        content = textwrap.dedent(
            """
        [content](link)
        """
        )
        should_be = "link\nNone\ncontent\n\n"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)

    def test_autolink(self):
        content = textwrap.dedent(
            """
        http://link
        """
        )
        should_be = "http://link\nFalse\n\n"
        result = render(content, self.extensions)
        self.assertEqual(result, should_be)
