import os
import shutil
import tempfile
import unittest

from kooki.config.document import Document
from kooki.processor.extension import Extension


class TestExtension(unittest.TestCase):
    def setUp(self):
        self.current = os.getcwd()
        self.kooki_tmp = tempfile.mkdtemp()
        self.tmp = tempfile.mkdtemp()
        os.chdir(self.tmp)
        os.environ["KOOKI_DIR"] = os.path.join(self.kooki_tmp, ".kooki")

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)
        shutil.rmtree(self.kooki_tmp)
        os.environ["KOOKI_DIR"] = ""

    def test_load_extensions(self):
        os.chdir(self.kooki_tmp)
        os.makedirs(".kooki/jars/jar")

        with open(".kooki/jars/jar/extension_2.html", "w") as stream:
            stream.write("extension content")

        os.makedirs(".kooki/jars/jar/dir_1")
        with open(".kooki/jars/jar/dir_1/extension_3.html", "w") as stream:
            stream.write("extension content")

        os.makedirs(".kooki/jars/jar/dir_2")
        with open(".kooki/jars/jar/dir_2/extension_3.html", "w") as stream:
            stream.write("extension content")

        os.chdir(self.tmp)
        with open("extension_1.html", "w") as stream:
            stream.write("extension content")

        document = Document()
        document.jars = [os.path.join(self.kooki_tmp, ".kooki/jars/jar")]

        # path = os.path.join(self.tmp, '.', 'extension_1.html')
        # self.assertTrue('extension_1' in extensions)
        # self.assertEqual(extensions['extension_1'].path, path)
        #
        # path = os.path.join(self.kooki_tmp, '.kooki/jars/jar/extension_2.html')
        # self.assertTrue('extension_2' in extensions)
        # self.assertEqual(extensions['extension_2'].path, path)
        #
        # path = os.path.join(self.kooki_tmp, '.kooki/jars/jar/dir_1/extension_3.html')
        # self.assertTrue('dir_1' in extensions)
        # self.assertTrue('extension_3' in extensions['dir_1'])
        # self.assertEqual(extensions['dir_1']['extension_3'].path, path)
        #
        # path = os.path.join(self.kooki_tmp, '.kooki/jars/jar/dir_2/extension_3.html')
        # self.assertTrue('dir_2' in extensions)
        # self.assertTrue('extension_3' in extensions['dir_2'])
        # self.assertEqual(extensions['dir_2']['extension_3'].path, path)

    def test_extension_with_args(self):
        document = Document()
        document.metadata = {}

        path_1 = os.path.join(self.tmp, ".", "extension_1.html")
        with open(path_1, "w") as stream:
            stream.write("@arg0")
        extension_1 = Extension(document, path_1)

        result = extension_1("argument")
        self.assertEqual(result, "argument")

    def test_extension_two_levels(self):
        document = Document()

        path_1 = os.path.join(self.tmp, ".", "extension_1.html")
        with open(path_1, "w") as stream:
            stream.write("hello")
        extension_1 = Extension(document, path_1)

        document.metadata = {"extension_1": extension_1}

        path_2 = os.path.join(self.tmp, ".", "extension_2.html")
        with open(path_2, "w") as stream:
            stream.write("@extension_1()")
        extension_2 = Extension(document, path_2)

        result = extension_2()
        self.assertEqual(result, "hello")

    def test_extension_three_levels(self):
        document = Document()

        path_1 = os.path.join(self.tmp, ".", "extension_1.html")
        with open(path_1, "w") as stream:
            stream.write("hello")
        extension_1 = Extension(document, path_1)

        document.metadata = {"extension_1": extension_1}

        path_2 = os.path.join(self.tmp, ".", "extension_2.html")
        with open(path_2, "w") as stream:
            stream.write("@extension_1()")
        extension_2 = Extension(document, path_2)

        document.metadata = {"extension_1": extension_1, "extension_2": extension_2}

        path_3 = os.path.join(self.tmp, ".", "extension_3.html")
        with open(path_3, "w") as stream:
            stream.write("@extension_2()")
        extension_3 = Extension(document, path_3)

        result = extension_3()
        self.assertEqual(result, "hello")

    def test_extension_markdown(self):
        os.chdir(self.kooki_tmp)
        os.makedirs(".kooki/jars/jar")

        with open(".kooki/jars/jar/__text__.html", "w") as stream:
            stream.write("@text")

        with open(".kooki/jars/jar/__header__.html", "w") as stream:
            stream.write("<h1>@text</h1>")

        os.chdir(self.tmp)

        document = Document()
        document.jars = [os.path.join(self.kooki_tmp, ".kooki/jars/jar")]
        document.metadata = {}

        path_1 = os.path.join(self.tmp, ".", "extension_1.md")
        with open(path_1, "w") as stream:
            stream.write("# title")
        extension_1 = Extension(document, path_1)

        result = extension_1()
        self.assertEqual(result, "<h1>title</h1>")
