import unittest

from kooki.processor.common import apply_template


class TestTemplate(unittest.TestCase):
    def test_apply_data_with_string(self):
        metadata = {"variable": "value"}
        content = "@variable"

        result = apply_template(content, metadata)

        self.assertTrue(isinstance(result, str))
        self.assertEqual(result, "value")

    def test_apply_template_with_bad_type(self):
        metadata = 1
        content = 1
        with self.assertRaises(Exception):
            apply_template(content, metadata)
