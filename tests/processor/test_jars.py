import os
import shutil
import tempfile
import unittest

from kooki.config.document import Document
from kooki.processor.jars import search_file, search_jar


class TestJars(unittest.TestCase):
    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        self.document = Document()
        self.document.jars = ["jar"]
        os.chdir(self.tmp)
        os.environ["KOOKI_DIR"] = self.tmp

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)
        os.environ["KOOKI_DIR"] = ""

    def test_search_jar(self):
        os.makedirs("jars/jar")
        path = search_jar("jar")
        self.assertEqual(path, os.path.join(os.getcwd(), "jars/jar"))

    def test_search_jar_not_found(self):
        path = search_jar("jar")
        self.assertEqual(path, None)

    def test_search_file(self):
        os.makedirs("jars/jar")
        open("jars/jar/content.md", "w").close()
        path = search_file(self.document, "content.md")
        self.assertEqual(path, os.path.join(os.getcwd(),
                                            "jars/jar/content.md"))

    def test_search_file_in_local(self):
        open("content.md", "w").close()
        path = search_file(self.document, "content.md")
        self.assertEqual(path, os.path.join(os.getcwd(), ".", "content.md"))

    def test_search_file_not_found(self):
        path = search_file(self.document, "content.md")
        self.assertEqual(path, None)
