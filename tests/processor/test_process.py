import os
import shutil
import tempfile
import unittest

from kooki.config.document import Document
from kooki.processor import process_document
from kooki.processor.exception import MissingMetadata, MissingTemplate


class TestProcess(unittest.TestCase):
    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)
        os.environ["KOOKI_DIR"] = self.tmp

        os.makedirs("jars/jar")
        open("jars/jar/template.html", "w").close()
        open("jars/jar/extension.html", "w").close()
        open("jars/jar/one.two.three", "w").close()

        open("metadata.yaml", "w").close()

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)
        os.environ["KOOKI_DIR"] = ""

    def test_export_to_markdown_ok(self):
        document = Document()
        document.output = "output"
        document.template = "template"
        document.jars = ["jar"]
        document.metadata = ["metadata.yaml"]
        process_document(document)

    def test_export_to_markdown_missing_metadata(self):
        document = Document()
        document.output = "output"
        document.template = "template"
        document.jars = ["jar"]
        document.metadata = ["metadata2.yaml"]
        with self.assertRaises(MissingMetadata):
            process_document(document)

    def test_export_to_markdown_missing_template(self):
        document = Document()
        document.output = "output"
        document.template = "template2"
        document.jars = ["jar"]
        document.metadata = []
        with self.assertRaises(MissingTemplate):
            process_document(document)
