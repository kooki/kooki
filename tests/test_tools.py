import os
import shutil
import tempfile
import textwrap
import unittest

from kooki.tools import get_front_matter, read_file, write_file


class TestTools(unittest.TestCase):
    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_get_no_front_matter(self):
        content = textwrap.dedent(
            """\
            content
            """
        )
        front_matter, content_extracted = get_front_matter(content)
        self.assertEqual(front_matter, {})
        self.assertEqual(content_extracted, "content\n")

    def test_get_front_matter(self):
        content = textwrap.dedent(
            """\
            ---
            key: value
            ---
            content
            """
        )
        front_matter, content_extracted = get_front_matter(content)
        self.assertEqual(front_matter, {"key": "value"})
        self.assertEqual(content_extracted, "content\n")

    def test_get_separator_but_no_front_matter(self):
        content = textwrap.dedent(
            """\
            ---
            key: value
            content
            """
        )
        front_matter, content_extracted = get_front_matter(content)
        self.assertEqual(front_matter, {})
        self.assertEqual(content_extracted, "---\nkey: value\ncontent\n")

    def test_write_file(self):
        file_name = "file.py"
        content = "content of the file"
        write_file(file_name, content)
        self.assertTrue(os.path.exists(file_name))
        with open(file_name, "r") as stream:
            content_read = stream.read()
            self.assertEqual(content_read, content)

    def test_write_file_multi_level(self):
        file_name = "test1/test2/file.py"
        content = "content of the file"
        write_file(file_name, content)
        self.assertTrue(os.path.exists(file_name))
        with open(file_name, "r") as stream:
            content_read = stream.read()
            self.assertEqual(content_read, content)

    def test_read_file(self):
        file_name = "file.py"
        content = "content of the file"
        with open(file_name, "w") as stream:
            stream.write(content)
        content_read = read_file(file_name)
        self.assertEqual(content_read, content)

    def test_read_file_missing_file(self):
        file_name = "file.py"
        with self.assertRaises(RuntimeError):
            read_file(file_name)
