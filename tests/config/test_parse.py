import unittest

from kooki.config.parse import parse_document_rules


class TestRuleParser(unittest.TestCase):
    def test_parse_document_rule_one_document(self):
        config = {
            "output": "output",
            "jars": ["jar"],
            "template": "template",
            "metadata": ["metadata"],
        }

        rule = parse_document_rules(config)
        self.assertEqual(rule.output, "output")
        self.assertEqual(rule.context, ".")
        self.assertEqual(rule.template, "template")
        self.assertEqual(rule.jars, ["jar"])
        self.assertEqual(rule.metadata, ["metadata"])

    def test_parse_document_rules_default(self):
        config = {
            "default": {
                "output": "output",
                "jars": ["jar"]
            },
            "kooki": {
                "document_1": {
                    "context": "context_1",
                    "template": "template_1",
                    "metadata": ["metadata_1"],
                },
                "document_2": {
                    "context": "context_2",
                    "template": "template_2",
                    "metadata": ["metadata_2"],
                },
            },
        }

        document_rules = parse_document_rules(config)

        self.assertTrue("document_1" in document_rules)
        self.assertEqual(document_rules["document_1"].output, "output")
        self.assertEqual(document_rules["document_1"].jars, ["jar"])

        self.assertTrue("document_2" in document_rules)
        self.assertEqual(document_rules["document_2"].output, "output")
        self.assertEqual(document_rules["document_2"].jars, ["jar"])

    def test_parse_document_rules_specific(self):
        config = {
            "default": {
                "output": "output",
                "jars": ["jar"]
            },
            "kooki": {
                "document_1": {
                    "context": "context_1",
                    "template": "template_1",
                    "metadata": ["metadata_1"],
                },
                "document_2": {
                    "context": "context_2",
                    "template": "template_2",
                    "metadata": ["metadata_2"],
                },
            },
        }

        document_rules = parse_document_rules(config)

        self.assertTrue("document_1" in document_rules)
        self.assertEqual(document_rules["document_1"].template, "template_1")
        self.assertEqual(document_rules["document_1"].context, "context_1")
        self.assertEqual(document_rules["document_1"].metadata, ["metadata_1"])

        self.assertTrue("document_2" in document_rules)
        self.assertEqual(document_rules["document_2"].template, "template_2")
        self.assertEqual(document_rules["document_2"].context, "context_2")
        self.assertEqual(document_rules["document_2"].metadata, ["metadata_2"])
