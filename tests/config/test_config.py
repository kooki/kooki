import os
import unittest

from kooki.config import (get_kooki_dir, get_kooki_dir_jars,
                          get_kooki_jar_manager)


class TestConfig(unittest.TestCase):
    def test_env_not_set(self):
        os.environ["KOOKI_DIR"] = ""
        kooki_dir = get_kooki_dir()
        self.assertEqual(kooki_dir, os.path.expanduser("~/.kooki"))

    def test_env_set(self):
        os.environ["KOOKI_DIR"] = "/home"
        kooki_dir = get_kooki_dir()
        self.assertEqual(kooki_dir, "/home")
        os.environ["KOOKI_DIR"] = ""

    def test_env_dir_jars(self):
        os.environ["KOOKI_DIR"] = ""
        kooki_dir = get_kooki_dir_jars()
        self.assertEqual(kooki_dir, os.path.expanduser("~/.kooki/jars"))

    def test_get_kooki_jar_manager(self):
        os.environ["KOOKI_JAR_MANAGER"] = ""
        kooki_jar_manager = get_kooki_jar_manager()
        managers = ["https://gitlab.com/kooki/jar_manager/raw/master/jars.yml"]
        self.assertEqual(kooki_jar_manager, managers)

    def test_get_kooki_jar_manager_with_env(self):
        os.environ["KOOKI_JAR_MANAGER"] = """['jars.yml']"""
        kooki_jar_manager = get_kooki_jar_manager()
        self.assertEqual(kooki_jar_manager, ["jars.yml"])

    def test_get_kooki_jar_manager_with_env_multiple(self):
        os.environ["KOOKI_JAR_MANAGER"] = """['jars.yml', 'jars2.yml']"""
        kooki_jar_manager = get_kooki_jar_manager()
        self.assertEqual(kooki_jar_manager, ["jars.yml", "jars2.yml"])
