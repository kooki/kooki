import unittest

from kooki.config.check import check_config_format


class TestConfig(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_minimum(self):
        config = {"output": "name", "template": "template"}
        check_config_format(config)

    def test_with_all_fields(self):
        config = {
            "output": "name",
            "template": "template",
            "context": "context",
            "jars": [],
            "metadata": [],
            "toppings": [],
        }
        check_config_format(config)

    def test_no_simple_with_multi(self):
        config = {"output": "name", "template": "template", "default": {}}
        with self.assertRaises(Exception):
            check_config_format(config)

    def test_simple_multi(self):
        config = {"default": {}, "kooki": {}}
        check_config_format(config)

    def test_default_field_without_kooki_field(self):
        config = {"default": {}}
        with self.assertRaises(Exception):
            check_config_format(config)

    def test_multi(self):
        config = {
            "kooki": {
                "doc_1": {
                    "output": "hello",
                    "template": "template"
                }
            }
        }
        check_config_format(config)
