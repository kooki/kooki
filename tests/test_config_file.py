import os
import shutil
import tempfile
import textwrap
import unittest

from kooki.tools import (
    NoConfigFileFound,
    YamlErrorConfigFileBadType,
    YamlErrorConfigFileParsing,
    read_config_file,
)


class TestReadConfigFile(unittest.TestCase):
    def setUp(self):
        self.tmp = tempfile.mkdtemp()
        self.current = os.getcwd()
        os.chdir(self.tmp)

    def tearDown(self):
        os.chdir(self.current)
        shutil.rmtree(self.tmp)

    def test_read_config_file_no_file(self):
        config_file_name = "kooki.yaml"
        with self.assertRaises(NoConfigFileFound):
            read_config_file(config_file_name, must_exist=True)

    def test_read_config_file_bad_yaml(self):
        config_file_name = "kooki.yaml"
        bad_config_file = textwrap.dedent(
            """\
            test_creation
                - test
                - doekdoe: [
            """
        )
        with open(config_file_name, "w") as stream:
            stream.write(bad_config_file)
        with self.assertRaises(YamlErrorConfigFileParsing):
            read_config_file(config_file_name)

    def test_read_config_file_bad_type_yaml(self):
        config_file_name = "kooki.yaml"
        bad_type_config_file = textwrap.dedent(
            """\
            test_creation
            """
        )
        with open(config_file_name, "w") as stream:
            stream.write(bad_type_config_file)
        with self.assertRaises(YamlErrorConfigFileBadType):
            read_config_file(config_file_name)
