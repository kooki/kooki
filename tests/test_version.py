import re
import unittest

from kooki.version import __version__


class TestVersion(unittest.TestCase):
    def test_version(self):
        m = re.match(r"^[0-9]+\.[0-9]+\.[0-9]+$", __version__)
        self.assertEqual(m.group(0), __version__)
