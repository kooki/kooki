FROM alpine
MAINTAINER noel.martignoni@easymov.fr

RUN apk add --update --no-cache python3 git

ADD . /kooki
WORKDIR /kooki
RUN python3 ./setup.py install

WORKDIR /workspace
CMD ['kooki']