# Kooki [unmaintained]

[![build status](https://gitlab.com/kooki/kooki/badges/master/build.svg)](https://gitlab.com/kooki/kooki/commits/master)
[![coverage](https://gitlab.com/kooki/kooki/badges/master/coverage.svg?job=coverage)](https://kooki.gitlab.io/kooki/coverage)
[![PyPI version](https://badge.fury.io/py/kooki.svg)](https://badge.fury.io/py/kooki)

Kooki is a powerful template system that help you create documents with Markdown and YAML.
Kooki come with different extensions that let you generate HTML file, PDF based on Latex and even more rfom markdown.

Need more info, look at the documentation. [kooki.gitlab.io](http://kooki.gitlab.io/).

## Install

```
pip3 install kooki
```
